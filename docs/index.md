## A propos de moi

![](images/avatar-photo.jpg)

Bonjour! Je m'appelle **Louise**. Je suis inscrite en master en architecture à la Faculté d'Architecture ULB La Cambre Horta à Bruxelles. Cette année, j'ai choisi de travailler au sein de l'option **Architecture & Design**. L'historique de mes travaux pour ce cours sont consultables sur ce site. 



## Parcours 
Lors du deuxième quadrimestre de ma 3ème année au sein de la faculté, j'ai choisi, comme projet d’architecture, l'atelier Digital Fabrication Studio. Ce fut ma première expérience avec les machines numériques. Au deuxième quadrimestre, j'ai suivi un workshop sur la terre crue en collaboration avec BC materials. L'année suivante, j'ai travaillé au sein du Laboratoire d'Architecture Digital (LAD) à Flagey en tant qu’étudiante assistante.
Ces expériences ont nourri mon intérêt pour la matière ainsi que pour les nouveaux moyens de conceptions numériques.  



## Projet choisi - **TRANSLATION** by Alain Gilles, 2008

![resize gm convert -resize 600x600 bigimage.png smallimage.jpg]( images/sample-photo.jpg)


**Description de l’objet :**

Tout droit sorti de l’imagination du designer belge Alain Gilles (1970-…), le fauteuil nommé TRANSLATION, tire son nom de sa forme obtenue par une translation entre le haut et le bas. Mesurant 75 cm de large sur 66cm de long, il a une hauteur de  72 cm pour une hauteur d'assise de 38.5 cm. Il est réalisé en polyéthylène recyclable (PE) et est disponible en d'autres coloris (12couleurs). On peut le contempler, dans sa couleur bleue parsemée de petits points colorés, dans la salle principale du musée ADAM (Art & Design Atomium Museum) à la fin de l’exposition. 


**Pourquoi l’avoir choisi ?**

Il m’a d’abord séduite visuellement par ses courbes organiques. Sa texture m’a fait penser aux pastels à cause de l’aspect luisant du revêtement. Malgré une coupe droite sur le devant de l’objet, les autres arêtes, celles de l’intérieur de l’assise, ont été adoucies ce qui le rend visuellement confortable. Cela donne envie de s’asseoir dedans, c’est pour cela que je l’ai choisi en plus de son aspect ludique. Paradoxalement, le choix du matériau et la résistance que l’objet doit avoir pour supporter le poids d’un homme fait que l’objet n’est pas souple et donc moins confortable. Le fait qu’il soit réalisé à partir de matière recyclable a fini de me convaincre.
