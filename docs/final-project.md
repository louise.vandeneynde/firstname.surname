# Projet final
## 1. Présentation

**Lampe Diabolo**

![](images/Final_cover.png) 

Comme pour l’œuvre de référence (le fauteuil « Translation » tire son nom du mouvement de translation de haut en bas utilisé pour obtenir la forme de l’objet), le nom de mon objet fait référence à la forme de ma lampe. La lampe « diabolo » est le fruit de l’inspiration du fauteuil « Translation » d’Alain Gilles ; de ses courbes, de sa massivité et de sa simplicité. 
La lampe est composée de deux parties de formes identiques. La première, le socle, a comme particularité d’accueillir un trou utile afin de faire passer le fil de la lampe. La deuxième partie est pensée pour loger le soquet. Les deux sont solidaires grâce à un système d’accroche. C’est aussi possible de n’utiliser que la partie « douille » si on veut la poser horizontalement. Réalisée en PETG recyclé, l'utilisation d'une imprimante 3D permet des jeux de motifs et de transparence. Cela, en plus de sa forme, ainsi que le fait d’avoir deux parties distinctes, permet des usages multiples ainsi qu’une personnalisation de l’objet. 


**Gallerie**
![](images/FINAL_1.png)
![](images/FINAL_2.png)


**Fichiers**

Les fichiers utilisés ainsi que mon A4 explicatif sont disponibles sur [ce lien]( https://drive.google.com/drive/folders/1RWR-Vb64oHHl6whYKHT2G0n47KynpoLD?usp=sharing)



## 2. Recherches
**2.1. Analyse de l'objet**

Tout droit sorti de l’imagination du designer belge Alain Gilles (1970-…), le fauteuil, nommé **TRANSLATION**, tire son nom de sa forme obtenue par une translation entre le haut et le bas. Fabriqué par Qui Est Paul et réalisé en polyéthylène recyclable (PE), il se trouve dans la salle principale du musée ADAM (Art & Design Atomium Museum) à la fin de l’exposition. Sa couleur bleue parsemée de petits points colorés attire l’œil. Son design minimaliste post moderne, qui le rend intemporel, a été décliné dans une version pour les enfants (First Translation) et en sofa pouvant accueillir plusieurs personnes. Disponible en d’autres coloris (12couleurs), il a été teinté dans la masse, ce qui le rend résistant à l’eau. De plus une ouverture au niveau de l’assise permet de laisser s’écouler les eaux de pluie, de sorte qu’il peut être placé aussi bien à l’intérieur qu’à l’extérieur. Il est aussi résistant aux UV, griffures et est facile d’entretien tout en étant relativement léger ; c’est donc un bon investissement dans le temps. 
Outre sa forme obtenue par translation, il semble avoir été réalisé à l’aide d'un moulage mais on pourrait aussi imaginer, dans l’optique du travail à effectuer dans le cadre de la question d’architecture : Architecture et Design module 1, qu’il pourrait aussi être réalisé par soustraction de matière. 
![resize gm convert -resize 600x600 bigimage.png smallimage.jpg](images/sample-pic-2.jpeg) 


**2.2. Réinterprétation**

**2.2.1. Inspiration**

C'est le mot qui correspond à l'attitude que je souhaite prendre. Il va me permettre de comprendre mon processus de pensée et me guider. 

**- Qu'est ce qu'il dit ?**

Dans 3 des 4 dictionnaires proposés (le Wiktionnary , le Larousse, le Littré), la **première définition** pour expliquer ce qu’est **l’inspiration** est **physiologique**. Elle désigne « la phase de la respiration pendant laquelle l’air entre dans les poumons: l'aspiration. Avec l’expiration c'est un mouvement en opposé qui se produit lorsque l’air est expulsé ». Comme le dit le Littré: « l'inspiration introduit dans le poumon un nouvel air » 
On peut faire un **parallèle** avec ce terme « nouvel air » , avec **l’idée de « souffle »**. On parle alors d'un « souffle créateur qui anime" (Le Petit Robert) et « enthousiasme » (Larousse) autrement dit une « idée créatrice, élan créateur d'origine mystérieuse » (Wiktionnary) « idée, résolution spontanée, soudaine » (le Petit Robert)

**-Mais comment le provoquer ?** 

On doit "chercher l'inspiration" (Larousse). Cela peut être une « chose inspirée », « conseillée/suggéré par quelqu’un » (Le Littré) ou quelque chose et qui nous inspire. On parle alors « d’influence et d’investigation » (Le Petit Robert) « ce qui stimule l’intellect, des émotions et de la créativité » (Wiktionnary)


**-Pourquoi l'avoir choisi ?**

Autrement dit l'inspiration il faut la chercher! J'ai choisi ce mot car cette manière de travailler me parle: l'idée de faire des recherches qui mènent à une idée.  


**- Comment l'utiliser sur mon objet ? / Comment fonctionner par rapport à mon objet?**

Afin de réinterpréter mon objet, je vais d'abord faire des recherches sur celui puis sur d'autres objets qui me font penser à mon fauteil en portant une attention toute particulière à leur construction et mode de fabrication. Ce que j'aimerais conserver de mon objet c'est sa couleur particulière (bleu parsemée de taches de couleurs) car c'est la première chose qui m'a plue en le voyant et l'utilisation de matériaux de récupération utilisés pour le concevoir. 

Exemples: 
![](images/Projet-final-INSPIRATION-ConvertImage.png)

[Objets fabriqués à partir de planches de skateboard](https://mrmondialisation.org/19-recyclages-astucieux-dune-planche-a-roulette/)

[Etapes de fabrication d'une guitare réalisée à partir de crayons de couleur](https://www.momes.net/actualites/actualites-insolites/une-guitare-entierement-en-crayons-de-couleur-851751)


Le problème c'est que vouloir recréer un matériau pour garder l'aspect de mon objet c'est bien mais sans idée de la manière de le réinterpréter, c'est problématique. 
C'est pourquoi j'ai repris ma réflexion depuis le début en réanalysant mon objet et voici ce que j'en ai tiré:

![](images/Projet_final_Idee.png)

**2.3. Modélisation - Fusion 360**

**A. Pièce A: "porte douille"**
1. Créer une esquisse sur le plan horizontal et créer un cercle de 15cm de diamètre.
2. A une distance de 6,5cm, créer un plan de décalage horizontal.
3. Sur ce plan de décalage, créer une esquisse sur le plan horizontal et créer un cercle de 12cm de diamètre.
4. Utiliser l'outil "lissage" pour relier les deux cercles.
5. Arrondir le bas du premier cercle de 1cm en utilisant l'outil "congé" 
6. Créer une esquisse sur le deuxième cercle, celui du haut de 12cm, et dessiner un cercle de 8cm de diamètre pour créer un anneau extérieur. Sur cet anneau, créer deux lignes et deux carrés de 1cm sur 1cm sur ces dernières. 
7. Extruder un demi anneau et un carré de 1cm vers le haut.
8. Recréer une esquisse sur le plan horizontal avec un cercle de 8cm
9. Extruder ce cercle.
10. Créer une esquisse sur le plan horizontal : deux cercles, un de 8cm et un de 4,5cm.
11. Extruder l'anneau ainsi créé vers le bas.

![](images/Piece_douille.png)


**B. Pièce B: avec un petit trou pour faire passer le fil de la lampe quand elle est posée**

Les étapes 1 à 9 sont identiques

10. Créer un plan de décalage vertical
11. Sur ce plan de décalage, créer une esquisse et dessiner un rond de 0,8cm de diamètre.
12. Extruder ce rond vers l'intérieur de l'objet.

![](images/Piece_B.png)


**2.4. Réalisation**

Après ces réflexions, je me suis lancée. J'ai décidé de réaliser mon objet, une lampe, en impression 3D. J'ai choisi cette mise en oeuvre pour garder le coté massif et courbe de mon objet initial. 

Voilà ma première réalisation. Je l'ai faite sans tenir compte de l'aspect technique de la lampe dans le but unique de voir comment elle rendait volumétriquement et au niveau des dimensions à l'échelle 1/1. Il y a eu plusieurs problèmes: premièrement j'ai imprimé deux fois la même pièce ce qui rend l'emboîtement impossible, deuxièmement,comme mentioné précédemment, je n'ai pas tenu compte de son aspect technique et, troisièmement,il y a eu un problème avec les supports qui ont été très difficiles à enveler d'où le rendu moyen.
![](images/Projet_final_Lampe_V1.png)

Après validation de l'idée lors de la correction du 03/12. J'ai été acheter de quoi réaliser ma lampe (cable et douille) et j'ai relancé une impression qui cette fois tient compte de l'aspect technique de la lampe en rajoutant une partie pour tenir la douille et j'ai aussi mis des supports plus écartés ce qui a facilité leur enlèvement. 
![](images/Projet_final_Lampe_V2.png)


**2.5. Création d'un moule**

**- Premier essai: moule vertical**

Pour réaliser mon moule j'ai suivi [ce tuto](https://www.youtube.com/watch?v=KPIUD6jMnL0). Voici le résultat : ![](images/Moulage_V.png) 
Le souci c'est qu'il me semble impossible à démouler. Je vais retenter en créant mon moule dans l'autre sens: horizontalement. 

**- Deuxième essai: moule horizontal**

![](images/Moulage_H.png) 

Encore faut-il que le moule soit réalisé avec un filament résistant à la chaleur et assez souple pour un démoulage facile.

Grâce au moule, la dimension du **recyclage** peut être abordée.


**- Quel plastique utiliser ?** 

- Ma première approche a été d'utiliser des bouteilles en plastique. Malheureusement, le plastique s'est rétracté, j'ai dû poser une casserole dessus pour lui donner une forme. Je ne pense donc pas que ça serait adaptable dans un moule.
- Ensuite, j'ai récupéré le remplissage d'une de mes pièces (qui a eu un souci) dans l'idée de voir comment il se comporte lorsqu'il est confronté à la chaleur. 


**2.6. Petit retour en arrière...**

Après avoir imprimé la partie basse de la lampe, le socle, et l'avoir assemblé avec l'autre partie voici le résultat:
![](images/Lampe_v1.png) 

Je l'ai ensuite testée avec une ampoule, une LED E27, pour voir comment elle rend en condition réelle:
![](images/Lampe_v1_ampoule_normale.png)

La lumière est un peu aveuglante sous certains angles, du coup j'en ai ensuite essayé une autre, toujours une LED mais avec un bout argenté; cela rend mieux.
![](images/Lampe_V1_ampoule_bout_argenté.png)


Mais cela ne change pas le fait que la lampe fait un peu massive. Une version plus "slim" est donc en cours. J'ai d'abord pensé à rajouter 2,5cm en hauteur pour chaque partie et arrondir le bout. Voici différentes vues de ce à quoi cela va ressembler: 

![](images/Vues_lampe.png)


Arrondir le bout "comme un donuts" allège visulemment la lampe mais ce n'est pas suffisant. J'ai donc décidé de repenser la lampe avec un plus petit soquet compatible avec une douille E14.


Un test avec un bord tronqué droit est aussi en cours pour essayer une version de la lampe posée de profil.


**2.7. Assemblage**

Premièrement il faut dévisser le soquet. Puis, il faut passer le cable dans la partie inférieur du soquet puis passer les deux cables et les visser. On peut ensuite visser ces deux parties ensemble avec la 3ème partie du soquet. 

![](images/Assemblage_1.png)

Après cela il faut emboîter les deux morceaux de la lampe. Par la suite, il faut passer le soquet dans la lampe et le maintenir grâce à la dernière pièce: l'anneau. Cela fait, il suffit de mettre une ampoule (une LED de préférence pour éviter qu'elle ne chauffe trop et donc ne fasse fondre le plastique) et de l'allumer. 
![](images/Assemblage_2.png)
