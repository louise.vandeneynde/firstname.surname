# 1. Gitlab

Le jeudi 08 octobre, j'ai assisté à deux modules: GitLab et Fusion360. Le 1er a eu lieu au matin, c’était une introduction au logiciel GitLab qui permet de centraliser différents travaux. J’ai appris le fonctionnement du site, les rudiments du code, comment effectuer des changements sur ma page personnelle,… 


## Processus de recherche : erreurs et succès
**1. Page personnelle**

Afin de réaliser ma page, j’ai d’abord dû retrouver comment l’éditer. Je me suis d’abord trompée en allant dans « setting », « page » et « acces pages ». Je me suis rendue compte qu’il fallait que je modifie l’index dans « index.md ». Puis en appuyant sur « edit », j’ai tapé mon texte et mis les mots importants en gras en utilisant des doubles astérisques. J’ai d’abord mis des espaces entre les astérisques et le mot à mettre en gras et, voyant que cela ne marchait pas après avoir appuyé sur « commited changes », j’ai modifié en collant bien les astérisques avec le mot au milieu.

Après cela j’ai mis des images en les remplaçant dans le dossier image. Malheureusement je n’ai pas réussi à changer leur taille. J’ai essayé en utilisant « gm convert -resize 600x600 bigimage.png smallimage.jp » comme indiqué dans le fichier « documentation.md ». Pour finir j'ai choisi de réduire la taille des mes images avec un [logiciel tiers](https://convertir-une-image.com/edition/reduire-redimensionner-une-photo.asp?i=20201013-154329-lqhcc). 
Pour mettre plusieurs images en bandes, j'ai les ai mises en page sur GIMP puis fait une capture d'écran du résultat. Je n'ai pas réussi à utiliser le package GraphicsMagick: "gm convert +append -geometry x400 image1.png image2.png image3.png image4.png strip.png"

Je me suis rendue compte que j'avais effectué tous ces changements seulement sur https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/louise.vandeneynde/-/tree/master/docs mais je ne l’avais pas fait sur https://gitlab.com/louise.vandeneynde/firstname.surname du coup j’ai effectué les modifications sur la deuxième page.

Un autre souci que j’ai rencontré c’est, qu’au début,quand je voulais ouvrir le fichier « docs », j’appuyais en fait sur le lien « last commit ».


**2. Git**
 
 Git est un logiciel de contrôle de versions décentralisées. Il est très intéressant à utiliser notamment lorsque l'on est plusieurs à travailler sur un même projet. Néanmoins, il est aussi utile lorsqu'on travaille seul car il permet, lorsqu'on a un travail avec plusieurs versions, de garder de l'ordre dans tout cela et de gagner du temps car on ne doit pas éditer les textes dans une nouvelle page. 

Etant sous macOS, j'ai d'abord essayé de le télécharger avec Xcode. Pour cela j'ai dû télécharger Xcode: une application créée pour les développeurs pour programmer facilement. Puis j'ai ensuite suivi les instructions de [ce site](https://medium.com/@bachur.nicolas/git-xcode-une-belle-histoire-utilisation-de-linterface-graphique-intégrée-5d26bfd6c9cd) afin de le "coupler" avec Git. J'ai réussi à aller jusqu'à l'étape "Création d’un repo distant depuis Xcode" mais je n'ai jamais réussi à créer le repo distant. Les étapes que j'ai suivies sont illustrées ci-dessous.

![](docs/images/MOD1.1.png)
![](docs/images/MOD1.2.png)
![](docs/images/MOD1.3.png)
![](docs/images/MOD1.4.png)


Puis j'ai téléchargé et essayé avec [Homebrew]( https://brew.sh/index_fr). J'ai copié/collé la ligne de code (/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)" dans mon terminal, tapé mon mot de passe et attendu mais rien ne s'est passé. 

![](docs/images/MOD1.5.png)


Finalement, j'ai téléchargé le logiciel [GitHub Desktop](https://desktop.github.com). Après m'être créée un compte, j'ai cloné mon adresse HTTPS. Delà, j'ai pu ouvrir Atom. 

![](docs/images/MOD_1.6.png) ![](docs/images/MOD1.7.png) 



# Quelques trucs appris sur le code:
- Pour mettre un tritre : # ; apparaît en mauve
- Pour mettre en gras : ** **
- Pour transformer un mot en lien : [mot] (lien); les deux doivent apparaître en rouge (merci Alizée pour cette astuce!)
- Pour faire une liste : mettre des tirets ; apparaît en mauve
- Pour mettre une ligne : mettres 3 tirets qui se suivent; apparaît en mauve
- Pour mettre un image : ![](lien de l'image trouvable dans le dossier "image")
