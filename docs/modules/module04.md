 # 4. Découpe assistée par ordinateur

Ce jeudi 15 octobre, j'ai assisté à la formation sur la découpe laser.

J'ai d'abord appris quels matériaux privilégier (multiplex, plexi, papier, carton, textile) pour la découpe et lesquels éviter: ceux qui font de la fumée épaisse/acide/nocive, qui font de la poussière nocive, qui fondent facilement, ceux impossibles à découper, qui réfléchissent,... Le choix du matériau est aussi régi par la taille de la machine: surface de découpe 122x61cm avec une hauteur maximum de 12cm. 

Avant d'utiliser la machine il faut toujours ouvrir la vanne d'air comprimé (on entend une sifflement quand elle est ouverte), allumer l'extracteur de fumée (bouton vert) et le refroidisseur à eau (bouton noir). Si ces étapes n'ont pas été respectées et/ou si la machine n'est pas fermée, elle marchera "à blanc". Pour l'allumer il faut tourner le bouton d'arrêt d'urgence dans le sens des flèches. Cela dévérouille le bouton, il suffira d'appuyer dessus s'il faut tout arrêter en urgence. 


## Processus de recherche : erreurs et succès

Afin de tester la machine, il nous a été demandé de réaliser une lampe, en rapport avec l'objet que l'on a choisi au musée, à partir d'une planche de plexi translucide semi-rigide. 

**1. Idée**

J'ai d'abord réfléchi à la forme de la lampe. L'idée était de partir de la forme du profil du fauteuil. 
![](docs/images/M4.Idee.jpg) 

**2. Fusion360**

Afin d'avoir les bonnes courbes, j'ai décidé d'enregistrer mon modèle sur fusion en PDF afin de pouvoir l'ouvrir sur AutoCAD. Pour cela je suis d'abord passeé en mode "dessin" dans fusion puis j'ai sorti le dessin en PDF. 
![](docs/images/M4.1.png) 

**3. AutoCAD**

Cela fait, j'ai ouvert le PDF sur AutoCAD, je l'ai nettoyé afin de garder que les traits qui m'intéressaient et j'ai généré mon modèle de découpe. Enfin, je l'ai exporté au format "DFX AutoCAD R12/LT2 (*.dxf)".
![](docs/images/M4.2.png) 

**4. Illustrator**

J'ai ouvert le fichier sur Illustrator. J'ai d'bord dû le "dégrouper" afin de supprimer les parties qui ne m'intéressaient pas en faisant "clic droit" puis " dissocier". Enfin, je l'ai enregistré au format SVG compatible avec le logiciel de découpe 3D. 
Un tout grand merci à Nigel pour son aide (à deux reprises) pour cette étape et pas que :) 
![](docs/images/M4.3.png) 

**5. Découpeuse laser**

De là, je l'ai importé sur l'ordinateur connecté à la découpeuse laser via une clé USB. D'abord, j'ai dû supprimer le travail du précédent utilisateur. Pour cela, j'ai cliqué sur le menu déroulant puis sur "clean" puis j'ai ouvert mon fichier en cliquant sur "open" (1). Une fois ouvert, j'ai pu configurer mes passages "Pass 1" et "Pass 2" (2) avec des couleurs et des puissances et/ou vitesses différentes. On met notamment des passages différents lorsqu'on a une partie à graver et l'autre à découper mais, dans mon cas, c'était pour éviter que la pièce ne bouge. Puis, j'ai déplacé la tête de la laser (point contour rouge (4)) en haut à droite du dessin (point rouge (5)) après avoir cliqué sur "Move" (3). Remarque: le bouton maison permet de placer la tête à l'origine. Enfin j'ai vérifié les statuts (sur l'image ils sont en orange car j'avais laissé la porte ouverte) et cliqué sur "runbounding box" (outil pour dessiner à blanc le contour du dessin) pour voir si le dessin rentrait bien sur ma planche. Enfin j'ai cliqué sur "run" (6) et la découpe s'est lancée. Elle a été plutôt rapide.
 ![](docs/images/Decoupe_laser.jpg) 

 **6. Résultat de la découpe**

 J'ai réalisé deux découpes, une petite (taille A4) et une grande (taille A3). J'ai commencé par la petite, la pièce ronde est tombée dans la machine avant d'être finie. J'ai alors recommencé mon dessin en plus grand. 
 ![](docs/images/M4.4.png) 

 **7. Résultat final**

J'ai finalement utlisé les petites pièces et la grande pièce ronde pour l'assemblage. Enfin, j'ai placé une ampoule d'une guirlande de noël pour rendre la maquette réaliste.
 ![](docs/images/M4.5-ConvertImage.png)  
