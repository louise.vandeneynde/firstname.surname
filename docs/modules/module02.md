# 2. Conception assistée par ordinateur
Le jeudi 08 octobre, j'ai assisté à deux modules: GitLab et Fusion360. 
L’après-midi, j’ai assisté à un cours sur le logiciel Fusion360. J’ai d’abord appris à créer une esquisse puis, grâce à divers exercices, à utiliser les outils mis à disposition tels que :

-	« extrusion» en choisissant l’objet ainsi que la direction de l’extrusion mais aussi en la répliquant dans un autre sens en choisissant l’axe approprié. 

-	« congé » qui arrondit les bords d’un objet à appliquer à la fin de l’exercice.

-	« révolution » qui fait pivoter le contour d’une esquisse sur un axe afin de créer un volume.

-	« coque » qui crée un bord en supprimant de la matière à l’intérieur d’une face

-	« balayage » qui crée une forme en reliant une surface plane avec une ligne 

-	« lissage » qui crée une forme de transition entre deux surfaces planes de formes différentes

-	…

Enfin, j’ai appris à changer d’espace de travail, à modifier la matière d’un objet, à faire un rendu et à afficher les dessins dans une mise en page en plan avec cartouche.


## Processus de recherche : erreurs et succès
Afin de réaliser mon modèle pour mon impression 3D, j'ai visioné l'enregistrement de la formation Fusion360. J'ai refais les excercices de la pièce d'échec et le dernier excercice, après je me suis lancée. 

Après, on a du se lancer dans la conception, sur le logiciel Fusion360, de l’objet que l’on a choisi au musée. 
Grâce à mes précédentes recherches, j’avais déjà les dimensions de mon objet, le fauteuil « Translation » d’Alain Gilles : 75 x 66 x 72 cm  (lxLxH) et 38.5 cm d’hauteur d'assise. J’ai néanmoins fait d’autres recherches pour trouver d’autres informations nécessaires pour commencer la conception. J’ai trouvé un modèle 3D du fauteuil sur [ce site](https://www.fonctionmeuble.com/catalogue/fauteuil-translation/?fbclid=IwAR2SySQMcAB3Hv-1T4zoKOZD_aX8aA4ZejmhTNzHhH5lVR14FbtF9Ud7J-g). Après l’avoir ouvert sur le logiciel « SketchUp », j’ai commencé à coter le modèle. Ce dernier étant tout en courbes, cela a été assez compliqué. J’ai donc essayé de l’ouvrir sur Fusion360 mais il s’est avéré être rempli de lignes (voir image ci-dessous). Du coup, j'ai abandonné l'idée et j'ai commencé, tant bien que mal,le module sur Fusion. J'ai d'abord modélisé le fauteuil en ne tenant pas compte des courbes. Le problème c'est que, une fois le modèle fini, je n'arrive pas à courber les arretes et ce même en utilisant l'outil "congé". Suite à des explications de Cassandra, j'ai enfin réussi à utiliser l'outil "congé" et, donc, à rendre mon modèle plus courbe (voir image ci-dessous). 

![](docs/images/MOD2.0.png) 


Le rendu final est plutôt satisfaisant mais je n'ai pas réussi à faire les courbes que je voulais, je pense que c'est parce que mon modèle a été été réalisé de manière trop complexe comme le montre la timeline ci-dessous. 

![](docs/images/MOD2.0bis.png) 


J'ai quand même imprimé le modèle histoire de tester l'imprimante 3D. Les étapes de l'impresion sont consultable dans
[le module 3](https://gitlab.com/louise.vandeneynde/firstname.surname/-/blob/master/docs/modules/module03.md). Elles ont été réalisées à partir d'un nouveau modèle, plus simple et qui rend mieux que j'ai créé plus tard et dont les étapes sont décrites ci-dessous.


![](docs/images/MOD2F.png) 



N'étant pas satisfaite du résultat, j'ai recommencé mon modèle fusion depuis le début en prenant, cette fois ci, de grandes cotes sur le SketchUp. J'ai d'abord crée des esquisses de chaque partie du modèle puis j'ai les ai extrudé. 

![](docs/images/MOD2.1.png)
![](docs/images/MOD2.2.png)

Le modèle étant symétrique, j'en ai dessiné la moitié puis j'ai utilisé l'outil miroir pour avoir un modèle complet. Puis, j'ai utilisé l'outil "combiné" pour n'avoir plus qu'un seul objet. Ensuite, j'ai congé toutes les façaces/arrêtes afain de rendre mon modèle courbe, le plus fidèle possible à l'original. Pour finir, j'ai crée le petit trou qui sert à l'écoulement des eaux. 

![](docs/images/MOD2.3.png)
![](docs/images/MOD2.4.png)

