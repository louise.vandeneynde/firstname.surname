# 5. Usinage assisté par ordinateur


## 1. Mise en route et scan

Pour allumer la Shaper, il suffit de la brancher. Il faut aussi penser à brancher un aspirateur. Il faut ensuite placer du "Shaper Tape" devant la machine afin qu'elle puisse se repérer. Enfin, il faut appuyer sur "scan" et bouger la Shaper pour qu'elle reconnaisse et enregistre le tape. 

Remarque: il faut au moins 4 dominos complets pour que la machine enregistre le tape.
![](docs/images/M5.1.png)

## 2. Dessiner

On peut ensuite passer au dessin en selectionnant "dessiner" à droite. On peut soit directement dessiner avec la machine en appuyant sur "créer" ou importer un fichier via une clé usb en appuyant sur "importer". Une fois le fichier importé, il s'afffiche à l'écran et on peut le mettre en place (le tourner, changer son échelle,...) jusqu'à avoir le résultat souhaité.
![](docs/images/M5.2.png)

## 3. Fraiser

Une fois tous ces réglages effectués, on peut lancer le fraisage en selectionnant "fraiser" à droite. Ce menu offre plusieurs possibilités: changer l'épaisseur/profondeur du fraisage, changer la dimension de la fraise, choisir comment on fraise: à l'extérieur, à l'intérieur ou sur la ligne ou encore créer une "poche" qui permet de fraiser complétement l'intérieur d'un dessin. Cela fait, il faut faire une calibration en Z, "Z touch", afin que la machine enregistre la distance entre la fraise et le support et c'est parti! La Shaper étant une sorte de CNC manuelle il faut la guider pour qu'elle fraise. 
![](docs/images/M5.3.png)

Petit plus: pour changer de fraise il faut dévisser l'embout qui tient la fraise, à l'aide d'une clé octogonale, le retirer, désserer le bouton pour pouvoir retirer la fraise puis tout resserer.
